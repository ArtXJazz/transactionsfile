package org.artx.transactions.file.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Artem Bogorodychenko
 * @company UnitedThinkers
 * @since 2020/12/01
 */

@XmlRootElement(name = "client")
@XmlAccessorType(XmlAccessType.FIELD)
public class Client implements Serializable {

	private static final long serialVersionUID = 6101726237726151459L;
	
    public Client() {
        super();
    }
 
    public Client(String firstName, String middleName, String lastName, String inn) {
        super();
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.inn = inn;
    }
	
	@XmlElement
	private String firstName;
	
	@XmlElement
	private String middleName;
	
	@XmlElement
	private String lastName;
	
	@XmlElement
	private String inn;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getInn() {
		return inn;
	}

	public void setInn(String inn) {
		this.inn = inn;
	}
}
