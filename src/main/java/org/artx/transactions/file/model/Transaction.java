package org.artx.transactions.file.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Artem Bogorodychenko
 * @company UnitedThinkers
 * @since 2020/12/01
 */

@XmlRootElement(name = "transaction")
@XmlAccessorType(XmlAccessType.FIELD)
public class Transaction implements Serializable {

	private static final long serialVersionUID = 6101726237722153459L;
	
	@XmlElement
	private String place;

	@XmlElement
	private String amount;
	
	@XmlElement
	private String currency;
	
	@XmlElement(name = "card")
	private String cardMasked;
	
	@XmlElementRef
	private Client client;
	
    public Transaction() {
        super();
    }
 
    public Transaction(String place, String amount, String currency, String cardMasked, Client client) {
        super();
        this.place = place;
        this.amount = amount;
        this.currency = currency;
        this.cardMasked = cardMasked;
        this.client = client;
    }
	
	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCardMasked() {
		return cardMasked;
	}

	public void setCardMasked(String cardMasked) {
		this.cardMasked = cardMasked;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
}
