package org.artx.transactions.file.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

/**
 * @author Artem Bogorodychenko
 * @company UnitedThinkers
 * @since 2020/12/01
 */
public class FileHelper {

	private static final int BUFFER_SIZE = 1024;
	
	private FileHelper() {
	}
	
	public static InputStream getFileContent(String filename) {
		
		if (filename == null || filename.trim().isEmpty()) {
            return null;
        }
		
		File file = new File(filename);
		if (!file.exists()) {
			return null;
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (BufferedInputStream bufferedInput = new BufferedInputStream(Files.newInputStream(file.toPath()))) {

   			int length;
   			byte[] buffer = new byte[BUFFER_SIZE];
    		while ((length = bufferedInput.read(buffer)) > 0) {
    			baos.write(buffer, 0, length);
    		}

    		baos.flush();

        } catch (IOException e) {
        	e.printStackTrace();
            return null;			
		}

        return new ByteArrayInputStream(baos.toByteArray());
	}
}
