package org.artx.transactions.file.service;

import java.io.InputStream;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.artx.transactions.file.model.GetTransactionsResponse;
import org.artx.transactions.file.model.TransactionResponse;

/**
 * @author Artem Bogorodychenko
 * @company UnitedThinkers
 * @since 2020/12/01
 */

public class TransactionsServiceImpl implements TransactionsService {
    
	private static final String EMPTY_RESULT = "Empty";
	private static final String DONE_RESULT = "Done";
	private static final String ERROR_RESULT = "Error";
	
	private static final String GET_TRANSACTIONS_RESPONSE_TAG = "GetTransactionsResponse";

	@Override
	public String submitResponses(EntityManager entityManager, GetTransactionsResponse transactionResponses) {
		
		if (transactionResponses == null || transactionResponses.getTransactions() == null 
				|| transactionResponses.getTransactions().isEmpty()) {
			
			return EMPTY_RESULT;
		}
		
		try {
		
			entityManager.getTransaction().begin();
			transactionResponses.getTransactions().forEach(t -> entityManager.persist(TransactionResponse.Factory.fromTransaction(t)));
			entityManager.getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			entityManager.getTransaction().rollback();
			return ERROR_RESULT;
		}
		
		return DONE_RESULT;
	}

	@Override
	public GetTransactionsResponse parse(InputStream inputStream) {

		if (inputStream == null) {
			return null;
		}
		
		try {
			
			XMLInputFactory xif = XMLInputFactory.newFactory();

		    XMLStreamReader xsr = xif.createXMLStreamReader(inputStream);
		    xsr.nextTag();

		    JAXBContext context = JAXBContext.newInstance(GetTransactionsResponse.class);
		    Unmarshaller unmarshaller = context.createUnmarshaller();
		    while (xsr.hasNext() && xsr.isStartElement()) {
		    	
		    	if (GET_TRANSACTIONS_RESPONSE_TAG.equalsIgnoreCase(xsr.getName().getLocalPart())) {
		    		return (GetTransactionsResponse) unmarshaller.unmarshal(xsr);
		    	}
		    	xsr.nextTag();
		    }
			
		} catch (JAXBException | XMLStreamException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
