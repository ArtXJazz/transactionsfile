package org.artx.transactions.file.service;

import java.io.InputStream;

import javax.persistence.EntityManager;

import org.artx.transactions.file.model.GetTransactionsResponse;

/**
 * @author Artem Bogorodychenko
 * @company UnitedThinkers
 * @since 2020/12/01
 */
public interface TransactionsService {
	
	GetTransactionsResponse parse(InputStream inputStream);
	
	String submitResponses(EntityManager entityManager, GetTransactionsResponse transactionResponses);

}
