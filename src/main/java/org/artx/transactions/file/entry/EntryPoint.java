package org.artx.transactions.file.entry;

import java.io.InputStream;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.artx.transactions.file.model.GetTransactionsResponse;
import org.artx.transactions.file.service.TransactionsService;
import org.artx.transactions.file.service.TransactionsServiceImpl;
import org.artx.transactions.file.utils.FileHelper;

/**
 * @author Artem Bogorodychenko
 * @company UnitedThinkers
 * @since 2020/12/01
 */
public class EntryPoint {

	public static void main(String[] args) {
		
		if (args == null || args.length == 0 || args[0].isEmpty()) {
			
			System.out.println("Specify the transaction response file as the first parameter");
			return;
		}

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("transactionsDS");
        try {
	        
        	EntityManager em = emf.createEntityManager();
	
        	try {
        		
        		InputStream inputStream = FileHelper.getFileContent(args[0]);
        		
        		TransactionsService ts = new TransactionsServiceImpl();
        		
		        GetTransactionsResponse transactionResponses = ts.parse(inputStream);
		        String result = ts.submitResponses(em, transactionResponses);
				
		        System.out.println(result);
	        
        	} catch (Exception e) {
				e.printStackTrace();
			} finally {
        		em.close();
        	}
        } finally {
        	emf.close();
        }
	}

}
