package transactions.file;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ FileHelperTest.class, TransactionServiceImplTest.class })
public class AllTests {

}
