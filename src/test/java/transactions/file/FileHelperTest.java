package transactions.file;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;

import org.artx.transactions.file.utils.FileHelper;
import org.eclipse.persistence.sessions.Project;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FileHelperTest {
	
	private static final int BUFFER_SIZE = 1024;
	private String tempFileName; 

	@Before
	public void setUp() throws Exception {
		
		File tempFile = File.createTempFile("test.xml", null);
		tempFileName = tempFile.getAbsolutePath();
		
		try (InputStream testFile = Project.class.getClassLoader().getResourceAsStream("test.xml");
			 OutputStream fos = Files.newOutputStream(tempFile.toPath());) {
			
   			int length;
   			byte[] buffer = new byte[BUFFER_SIZE];
    		while ((length = testFile.read(buffer)) > 0) {
    			fos.write(buffer, 0, length);
    		}

    		fos.flush();
		}
	}

	@After
	public void tearDown() throws Exception {
		
		File tempFile = new File(tempFileName);
		if (tempFile.exists()) {
			tempFile.delete();
		}
	}

	@Test
	public void testGetFileContent() {
		
		Assert.assertNull("FileName is Null", FileHelper.getFileContent(null));
		Assert.assertNull("FileName is Empty", FileHelper.getFileContent(""));
		Assert.assertNull("File not exists", FileHelper.getFileContent("zzz"));
		Assert.assertNotNull("Valid File", FileHelper.getFileContent(tempFileName));
	}

}
