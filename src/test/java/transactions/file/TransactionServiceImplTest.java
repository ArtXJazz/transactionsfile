package transactions.file;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.artx.transactions.file.model.Client;
import org.artx.transactions.file.model.GetTransactionsResponse;
import org.artx.transactions.file.model.Transaction;
import org.artx.transactions.file.service.TransactionsService;
import org.artx.transactions.file.service.TransactionsServiceImpl;
import org.eclipse.persistence.sessions.Project;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class TransactionServiceImplTest {

	private static final String EMPTY_RESULT = "Empty";
	private static final String DONE_RESULT = "Done";
	
	private TransactionsService ts; 
	
	@Before
	public void setUp() throws Exception {
		ts = new TransactionsServiceImpl();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSubmitResponses() {

		EntityManager em = Mockito.mock(EntityManager.class);
		Mockito.when(em.getTransaction()).thenReturn(Mockito.mock(EntityTransaction.class));
		
		Assert.assertTrue("Responses is Null", EMPTY_RESULT.equalsIgnoreCase(ts.submitResponses(em, null)));
		
		GetTransactionsResponse transactionResponses = new GetTransactionsResponse();
		Assert.assertTrue("Responses is Empty", EMPTY_RESULT.equalsIgnoreCase(ts.submitResponses(em, transactionResponses)));
		
		transactionResponses.setTransactions(new ArrayList<Transaction>());
		Assert.assertTrue("Responses is Empty", EMPTY_RESULT.equalsIgnoreCase(ts.submitResponses(em, transactionResponses)));
		
		transactionResponses.getTransactions().add(new Transaction("Test Place", "10.00", "UAH", "4111********1111", new Client("Ivan", "Petrovich", "Sidorov", "0123456789")));
		Assert.assertTrue("Responses is Good", DONE_RESULT.equalsIgnoreCase(ts.submitResponses(em, transactionResponses)));
	}

	@Test
	public void testParse() {
		
		Assert.assertNull("File is null", ts.parse(null));
		
		try (InputStream testFile = Project.class.getClassLoader().getResourceAsStream("test.xml");
			 InputStream emptyFile = Project.class.getClassLoader().getResourceAsStream("empty_test.xml");
			 InputStream corruptedFile = Project.class.getClassLoader().getResourceAsStream("corrupted_test.xml");) {
		
			Assert.assertNull("File is correct but empty", ts.parse(emptyFile));
			Assert.assertNull("File is corrupted", ts.parse(corruptedFile));
			Assert.assertNotNull("File is correct and contain transactions", ts.parse(testFile));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
